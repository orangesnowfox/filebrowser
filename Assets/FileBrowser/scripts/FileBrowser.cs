﻿//—————————————————————————————————————————————————————————————————————————————————————
//Copyright (c) 2016 Zachery Gyurkovitz https://opensource.org/licenses/mit-license.php
//—————————————————————————————————————————————————————————————————————————————————————

using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
namespace com.gyurkovitz.zachery.FileBrowser {
    /// <summary>
    /// the main bit of code that does everything.
    /// </summary>
    public static class FileBrowser {

        #region Varibles
        // FIXME: find a summary 
        static List<GameObject> DirectoryGOs = new List<GameObject>();

        //FIXME: find a summary
        static List<GameObject> FileGos = new List<GameObject>();

        /// <summary>
        /// a list of directories for the current root.
        /// </summary>
        static string[] Dirs;

        /// <summary>
        /// a list of files for the current root.
        /// </summary>
        static string[] Files;

        /// <summary>
        /// the currently selected path.
        /// </summary>
        static string selectedPath = "";

        /// <summary>
        /// the time since a directory button was last clicked (so that i know when to go up or down a level.)
        /// </summary>
        static float lastDirButtonClickedTime = 0;

        /// <summary>
        /// the Canvas for all the UI elements to sit in.
        /// </summary>
        static GameObject Canvas;

        /// <summary>
        /// if true the FileBrowser will not accept any requests until it becomes false again.
        /// </summary>
        static bool running = false;

        /// <summary>
        /// if true the FileBrowser will not do anything when the Prepare function is called.
        /// </summary>
        static bool prepared = false;

        /// <summary>
        /// the function to run when the user selects a file/directory cannot be null;
        /// </summary>
        static Action<string> onSucsess;

        /// <summary>
        /// the function to run when the user presses the cancel button.
        /// </summary>
        static Action onCancel;

        /// <summary>
        /// Will only show files if true, otherwise they will be discluded from the search.
        /// </summary>
        static bool showFiles = true;

        /// <summary>
        /// only used if there is no event system already in place.
        /// </summary>
        static GameObject EventSystem;

        /// <summary>
        /// The box where text goes above the FileBrowser.
        /// </summary>
        static GameObject DiologBox;

        /// <summary>
        /// The text to put in the DiologBox the user tries to use a Directory when they need to select a file (or vise versa.)
        /// </summary>
        static string DiologBoxInvalidChoice;

        #endregion Varibles

        #region Functions

        /// <summary>
        /// Prepares the FileBrowser to be ran next frame.
        /// </summary>
        /// <param name="root"> The Directory to start at. </param>
        /// <param name="OnSucsess"> This Function runs when the user selects a Directory/File
        /// (depending on which one you are trying to have the user select) and presses the 'Select' button. CANNOT BE NULL! </param>
        /// <param name="OnCancel"> This Function runs when the user presses the 'Cancel' button. </param>
        public static void Prepare(string root, Action<string> OnSucsess, Action OnCancel) {
            //Make sure the root directory exists.
            if (!Directory.Exists(root)) {
                throw new DirectoryNotFoundException(
                "the provided root (" + root + ") is not a valid Directory please provide a valid Directory");
            }
            //I don't like the idea of having a user select something and then nothing happens because a programmer set a Action to null
            //when it should be something else.
            if (OnSucsess == null) {
                throw new ArgumentNullException("OnSucsess", "OnSucsess can not be null!");
            }
            //The FileBrowser can't support two instances running at the same time.
            if (prepared) {
                throw new InvalidOperationException("Cannot have two FileBrowsers prepared at once!");
            }
            //The FileBrowser can't support two instances running at the same time.
            if (running) {
                throw new InvalidOperationException("Cannot prepare a FileBrowser while one is already running!");
            }
            //this is the part where we actually prepare for the FileBrowser to be ran next frame.
            prepared = true;
            selectedPath = root;
            Canvas = GameObject.Instantiate(Resources.Load("FileBrowser/Prefabs/UICanvas") as GameObject);
            Canvas.name = "UICanvas";
            onSucsess = OnSucsess;
            onCancel = OnCancel;
            if (UnityEngine.EventSystems.EventSystem.current == null) {
                EventSystem = GameObject.Instantiate(Resources.Load("FileBrowser/Prefabs/EventSystem") as GameObject);
                EventSystem.name = "Eventsystem";
            }
        }

        /// <summary>
        /// Starts the FileBrowser and also makes sure it will only finish if you select a Directory.
        /// </summary>
        /// /// <param name="diologBoxText"> Text to use in the diolog box default is: "Select a Directory please" </param>
        /// <param name="diologBoxTextInvalidChoice"> Text to use in the diolog box when the user tries to select a File
        /// default is: "Invalid choice please select a Directory!"</param>
        public static void selectDirectory(string diologBoxText = "", string diologBoxTextInvalidChoice = "") {
            if (running) {
                throw new InvalidOperationException("Cannot run two FileBrowsers at once!");
            }
            running = true;
            showFiles = false;
            //—————————————————————————————————————————————————————————————————————————————————————————
            diologBoxText = (diologBoxText == "" ? "Select a Directory please." : diologBoxText);
            diologBoxTextInvalidChoice = (diologBoxTextInvalidChoice == "" ?
            "Invalid choice please select a Directory!" : diologBoxTextInvalidChoice);
            DiologBoxInvalidChoice = diologBoxTextInvalidChoice;
            //this is a mess :/ wonder how to fix it
            //—————————————————————————————————————————————————————————————————————————————————————————
            Canvas.transform.FindChild("Select").gameObject.GetComponent<Button>().onClick.AddListener(() => OnFinishDir(true));
            Canvas.transform.FindChild("Cancel").gameObject.GetComponent<Button>().onClick.AddListener(() => OnFinishDir());
            Canvas.transform.FindChild("Diolog").FindChild("Text").gameObject.GetComponent<Text>().text = diologBoxText;
            recreateDirGos();
        }
        /// <summary>
        /// Starts the FileBrowser and also makes sure it will only finish if you select a File.
        /// </summary>
        /// <param name="diologBoxText"> Text to use in the diolog box default is: "Select a File please" </param>
        /// <param name="diologBoxTextInvalidChoice"> Text to use in the diolog box when the user tries to select a Directory
        /// default is: "Invalid choice please select a File!"</param>
        public static void selectFile(string diologBoxText = "", string diologBoxTextInvalidChoice = "") {
            if (running) {
                throw new InvalidOperationException("Cannot run two FileBrowsers at once!");
            }
            running = true;
            //—————————————————————————————————————————————————————————————————————————————————————————
            diologBoxText = (diologBoxText == "" ? "Select a File please." : diologBoxText);
            diologBoxTextInvalidChoice = (diologBoxTextInvalidChoice == "" ?
            "Invalid choice please select a File!" : diologBoxTextInvalidChoice);
            DiologBoxInvalidChoice = diologBoxTextInvalidChoice;
            //this is a mess :/ wonder how to fix it
            //—————————————————————————————————————————————————————————————————————————————————————————
            Canvas.transform.FindChild("Select").gameObject.GetComponent<Button>().onClick.AddListener(() => OnFinishFile(true));
            Canvas.transform.FindChild("Cancel").gameObject.GetComponent<Button>().onClick.AddListener(() => OnFinishFile());
            Canvas.transform.FindChild("Diolog").FindChild("Text").gameObject.GetComponent<Text>().text = diologBoxText;
            recreateDirGos();
        }

        /// <summary>
        /// Destroies and then recreates the Directory GameObjects. 
        /// </summary>
        static void recreateDirGos() {
            Dirs = Directory.GetDirectories(selectedPath);
            for (int dir = DirectoryGOs.Count - 1; dir >= 0; dir--) {
                GameObject.Destroy(DirectoryGOs[dir]);
            }
            if (!Directory.GetLogicalDrives().Contains(selectedPath.Replace('/', '\\'))) {
                GameObject Back = GameObject.Instantiate(Resources.Load("FileBrowser/Prefabs/ElementButton") as GameObject);
                DirectoryGOs.Add(Back);
                Back.transform.SetParent(Canvas.transform.FindChild("list").FindChild("content"));
                Back.name = "Back";
                Back.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
                ButtonOptions BackButtonOption = Back.AddComponent<ButtonOptions>();
                BackButtonOption.str = Directory.GetParent(selectedPath).FullName;
                Back.GetComponent<Button>().onClick.AddListener(() => dirButtonClick(BackButtonOption.str, true));
                Back.transform.FindChild("Text").gameObject.GetComponent<Text>().text = "../ ";
            }

            foreach (string dir in Dirs) {
                GameObject dirGo = GameObject.Instantiate(Resources.Load("FileBrowser/Prefabs/ElementButton") as GameObject);
                DirectoryGOs.Add(dirGo);
                dirGo.transform.SetParent(Canvas.transform.FindChild("list").FindChild("content"));
                string dirName = new DirectoryInfo(dir).Name;
                dirGo.name = dirName;
                dirGo.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
                ButtonOptions ButtonOption = dirGo.AddComponent<ButtonOptions>();
                ButtonOption.str = dir;
                dirGo.GetComponent<Button>().onClick.AddListener(() => dirButtonClick(ButtonOption.str));
                dirGo.transform.FindChild("Text").gameObject.GetComponent<Text>().text = dirName;
            }
            if (showFiles)
                recreateFileGos();
        }

        /// <summary>
        /// Destroies and then recreates the File GameObjects. 
        /// </summary>
        static void recreateFileGos() {
            Files = Directory.GetFiles(selectedPath);
            for (int file = FileGos.Count - 1; file >= 0; file--) {
                GameObject.Destroy(FileGos[file]);
            }
            foreach (string file in Files) {
                GameObject FileGo = GameObject.Instantiate(Resources.Load("FileBrowser/Prefabs/ElementButton") as GameObject);
                FileGos.Add(FileGo);
                FileGo.transform.SetParent(Canvas.transform.FindChild("list").FindChild("content"));
                string FileName = new FileInfo(file).Name;
                FileGo.name = FileName;
                FileGo.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
                ButtonOptions ButtonOption = FileGo.AddComponent<ButtonOptions>();
                ButtonOption.str = file;
                FileGo.GetComponent<Button>().onClick.AddListener(() => fileButtonClick(ButtonOption.str));
                FileGo.transform.FindChild("Text").gameObject.GetComponent<Text>().text = FileName;
            }
        }

        /// <summary>
        /// When a Directory button is pressed this checks to see if it should recreate the Directory GameObjects
        /// and also sets selectedPath to str.
        /// </summary>
        /// <param name="str"> The string to set selectedPath to. </param>
        /// <param name="recreateOnfirstClick"> Weather or not to recreate the Directory GameObjects no matter what. </param>
        static void dirButtonClick(string str, bool recreateOnfirstClick = false) {
            if (recreateOnfirstClick) {
                selectedPath = str;
                recreateDirGos();
                return;
            }
            if (Time.time - lastDirButtonClickedTime < 1f / 3f)
                recreateDirGos();
            lastDirButtonClickedTime = Time.time;
            selectedPath = str;
        }

        /// <summary>
        /// When a File button is pressed this sets selectedPath to str.
        /// </summary>
        /// <param name="str"> The string to set selectedPath to. </param>
        static void fileButtonClick(string str) {
            selectedPath = str;
        }

        /// <summary>
        /// When the FileBrowser is started with 'selectDirectory' and the 'Select' or 'Cancel' button is pressed this function
        /// checks to see if selectedPath is a valid Directory, if it is, or if sucsess was false, it will run OnFinsihCommonCode in different
        /// 'states'
        /// </summary>
        /// <param name="sucsess"> if false will allow OnFinishCommonCode to run with the 'Cancel' state, if true and selectedPath is a valid
        /// Directory it will run OnFinishCommonCode in the 'Sucsess' state. </param>
        static void OnFinishDir(bool sucsess = false) {
            if (Directory.Exists(selectedPath) || !sucsess)
                OnFinishCommonCode(sucsess);
            else
                Canvas.transform.FindChild("Diolog").FindChild("Text").gameObject.GetComponent<Text>().text = DiologBoxInvalidChoice;
        }

        /// <summary>
        /// When the FileBrowser is started with 'selectFile' and the 'Select' or 'Cancel' button is pressed, this function checks to see if
        /// selectedPath is a valid File, if it is, or if sucsess was false, it will run OnFinsihCommonCode in different 'states'.
        /// </summary>
        /// <param name="sucsess"> if false will allow OnFinishCommonCode to run with the 'Cancel' state, if true and selectedPath is a valid
        /// file it will run OnFinishCommonCode in the 'Sucsess' state. </param>
        static void OnFinishFile(bool sucsess = false) {
            if (File.Exists(selectedPath) || !sucsess) 
                OnFinishCommonCode(sucsess);
            else
                Canvas.transform.FindChild("Diolog").FindChild("Text").gameObject.GetComponent<Text>().text = DiologBoxInvalidChoice;
        }
        static void OnFinishCommonCode(bool sucsess) {
            Action<string> onSucsessTmp = onSucsess;
            onSucsess = null;
            Action onCancelTmp = onCancel;
            onCancel = null;
            string selectedPathTmp = selectedPath;
            selectedPath = string.Empty;
            showFiles = true;
            Dirs = null;
            Files = null;
            prepared = false;
            running = false;
            lastDirButtonClickedTime = 0f;
            GameObject.Destroy(Canvas);
            if (EventSystem != null) 
                GameObject.Destroy(EventSystem);
            if (sucsess)
                onSucsessTmp(selectedPathTmp);
            else if (onCancelTmp != null)
                onCancelTmp();
        }
        #endregion Functions
    }
}