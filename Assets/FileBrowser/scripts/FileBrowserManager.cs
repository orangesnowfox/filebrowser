﻿//—————————————————————————————————————————————————————————————————————————————————————
//Copyright (c) 2016 Zachery Gyurkovitz https://opensource.org/licenses/mit-license.php
//—————————————————————————————————————————————————————————————————————————————————————

using UnityEngine;
using System.Collections;
using System;
namespace com.gyurkovitz.zachery.FileBrowser {
    public class FileBrowserManager : MonoBehaviour {

        static bool UseFile;
        static string DiologBoxText;
        static string DiologBoxTextInvalidChoice;
        static bool isRequsting;
        static bool waited;
        
        void Update() {
            if (isRequsting) {
                if (waited) {
                    if (UseFile)
                        FileBrowser.selectFile(DiologBoxText, DiologBoxTextInvalidChoice);
                    else 
                        FileBrowser.selectDirectory(DiologBoxText, DiologBoxTextInvalidChoice);
                    isRequsting = false;
                } else
                    waited = true;
            }
        }
        /// <summary>
        /// Prepares the file browser for use next frame
        /// </summary>
        /// <param name="root"> The directory to start in </param>
        /// <param name="onSucsess"> The function to call when the user clicks the 'Select' button 
        /// CANNOT BE NULL </param>
        /// <param name="onCancel"> The function to call when the user clicks the 'Cancel' button </param>
        /// <param name="useFile"> Are we searching for files? </param>
        /// <param name="diologBoxText"> The text to put in the diolog box. leave empty for default text
        /// (depends on file or Directory search) </param>
        /// <param name="diologBoxTextInvalidChoice"> The text to put in the diolog box when the user tries 
        /// to do something invalid. leave empty for default text (depends on file or Directory search) </param>
        public static void Prepare(string root,Action<string> onSucsess,Action onCancel, bool useFile = false,
        string diologBoxText="", string diologBoxTextInvalidChoice="") {
            FileBrowser.Prepare(root, onSucsess, onCancel);
            UseFile = useFile;
            DiologBoxText = diologBoxText;
            DiologBoxTextInvalidChoice = diologBoxTextInvalidChoice;
            isRequsting = true;
            waited = false;
        }
    }
}